'use strict';

module.exports = function(grunt) {
  var pngquant = require('imagemin-pngquant'),
    jpegRecompress = require('imagemin-jpeg-recompress');

  var appConfig = {
    app: 'app',
    dist: 'dist',
    testurl: 'http://codevember.local'
  };

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      css: {
        files: [appConfig.app + '/css/scss/**/*.scss'],
        tasks: ['compass:dev', 'postcss:dev'],
        options: {
          atBegin: true
        }
      },
      livereloadrefresh: {
        files: [
          appConfig.app + '/**/*.css',
          appConfig.app + '/**/*.js',
          appConfig.app + '/**/*.json',
          appConfig.app + '/**/*.png',
          appConfig.app + '/**/*.jpg',
          appConfig.app + '/**/*.gif',
          appConfig.app + '/**/*.html',
          appConfig.app + '/**/*.php'
        ],
        options: {
          livereload: true
        }
      }
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            cwd: appConfig.app + '/',
            src: ['./*.php', '!./index.php', './**/*.svg', './**/*.ico', './**/*.xml'],
            dest: appConfig.dist + '/',
            filter: 'isFile'
          },
          {
            expand: true,
            cwd: appConfig.app,
            src: [
              'pdf/**',
              'php/**',
              'aud/**',
              'videos/**',
              'templates/**',
              'fonts/**',
              'favicons/**',
              'lib/**',
              'favicons/**',
              'json/**'
            ],
            dest: appConfig.dist + '/'
          }
        ]
      },
      noImages: {
        files: [
          {
            expand: true,
            cwd: appConfig.app + '/',
            src: ['./*.php', '!./index.php'],
            dest: appConfig.dist + '/',
            filter: 'isFile'
          },
          {
            expand: true,
            cwd: appConfig.app,
            src: ['php/**', 'aud/**', 'videos/**', 'templates/**', 'fonts/**', 'lib/**', 'favicons/**', 'json/**'],
            dest: appConfig.dist + '/'
          }
        ]
      }
    },
    uglify: {
      dist: {
        files: {}
      }
    },
    processhtml: {
      options: {
        // Task-specific options go here.
      },
      dist: {
        files: {
          'dist/index.php': ['app/index.php']
        }
      }
    },
    requirejs: {
      dist: {
        options: {
          baseUrl: appConfig.app + '/js',
          mainConfigFile: appConfig.app + '/js/main.js',
          name: '../../bower_components/almond/almond',
          include: ['main'],
          insertRequire: ['main'],
          out: appConfig.dist + '/js/main.js'
        }
      }
    },
    imagemin: {
      dynamic: {
        options: {
          // Target options
          optimizationLevel: 0,
          use: []
        },
        files: [
          {
            expand: true, // Enable dynamic expansion
            cwd: 'app/images/', // Src matches are relative to this path
            src: ['**/*.{png,jpg,gif}'], // Actual patterns to match
            dest: appConfig.dist + '/images/' // Destination path prefix
          }
        ]
      }
    },
    compass: {
      // Task
      dist: {
        // Target
        options: {
          // Target options
          sassDir: appConfig.app + '/css/scss',
          cssDir: appConfig.dist + '/css',
          outputStyle: 'compressed'
        }
      },
      dev: {
        // Target
        options: {
          // Target options
          sassDir: appConfig.app + '/css/scss',
          cssDir: appConfig.app + '/css'
        }
      }
    },
    optimize: 'uglify2',
    generateSourceMaps: false,
    preserveLicenseComments: false,
    uglify2: {
      preserveComments: false
    },
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({ browsers: ['last 2 version', 'ios >= 6', 'ie 8', 'ie 9', 'ie 10', 'ie 11'] })
        ]
      },
      dev: {
        src: appConfig.app + '/css/*.css'
      },
      dist: {
        src: appConfig.dist + '/css/*.css'
      }
    },
    open: {
      all: {
        path: appConfig.testurl,
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-open');

  grunt.registerTask('dev', ['open', 'watch']);

  // builds to dist -- live deployment
  grunt.registerTask('build', [
    'requirejs:dist',
    'imagemin',
    'compass:dist',
    'postcss:dist',
    'uglify:dist',
    'copy:dist',
    'processhtml:dist'
  ]);

  // builds to dist -- live deployment, but ignores the image compress if we are just doing code
  grunt.registerTask('build-no-images', [
    'requirejs:dist',
    'compass:dist',
    'postcss:dist',
    'uglify:dist',
    'copy:noImages',
    'processhtml:dist'
  ]);
};
