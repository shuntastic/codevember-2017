<?php
	function debug_to_console($data) {
	    if(is_array($data) || is_object($data))
		{
			echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
		} else {
			echo("<script>console.log('PHP: ".$data."');</script>");
		}
	}

	// if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['query'];
		// $human = intval($_POST['human']);
		$to = 'shun.smith@gmail.com';

		$headers = 'X-Mailer: PHP/'.phpversion().'\r\nContent-type: text/html; charset=iso-8859-1';


		$subject = 'Message to CoolMellon';
		$body = 'From: '.$name.'\r\nE-Mail: '.$email.'\r\nMessage: '.$message;
		// $body = wordwrap($bodyCopy, 70, '\r\n');
        $result = '';
		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
			$result = 'Please enter your name ';
		}
		
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
			$result .= 'Please enter a valid email address ';
		}
		
		//Check if message has been entered
		if (!$_POST['query']) {
			$errMessage = 'Please enter your message';
			$result .= 'Please enter your message';
		}
		// //Check if simple anti-bot test is correct
		// if ($human !== 5) {
		// 	$errHuman = 'Your anti-spam is incorrect';
		// }
 
        // If there are no errors, send the email
        // if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
        if (!$errName && !$errEmail && !$errMessage) {
			$sent = mail($to,$subject,$body,$headers);
            if ($sent) {
                $result='<div class="alert alert-success">Thank You! I will be in touch</div>';
            } else {
                $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
			}			
		}
		echo $result;
	// }
	return;
	
?>