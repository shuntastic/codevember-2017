/*jslint browser:true */
/*globals define, $, SITE, _, Expo, TimelineMax */

/**
 * The ThreeJS videos page of the website
 *
 *
 */
;(function (root, factory) {
    "use strict";
    root.app = root.app || {};
	var o = factory($);
	root.app.VideoPlayerMP4 = o;
}(window.SITE = window.SITE || {}, function () {

    function VideoPlayerMP4(params) {
		this.parent = params.parent;
		this.videoData = params.videoData;
		this.player = null;
		this.onReady = params.onReady;
		this.load();
	}
	VideoPlayerMP4.prototype = {

		load: function () {
			this.parent.empty().append('<div id="mp4Player"></div>');
			this.player = $('<video src="'+this.videoData.videoId+'" class="absFull" autoload controls preload="auto">');
			$('#mp4Player').html(this.player);
		},
		
		play: function () {
			if (this.player) {
				this.player.get(0).play();
			}
		},
		
		pause: function() {
			if (this.player && this.player.pause) {
				this.player.get(0).pause();
			}	
		},

		onPlayerReady: function () {
			this.onReady();
		},

		onStateChange: function(e) {
		},
		
		destroy: function() {
			this.parent.empty();
			if (this.player != null) {
				this.player = null;
			}
		},
		
		change: function(newData) {
			this.videoData = newData;
			this.load();
		}
	}
	
    return VideoPlayerMP4;
}));
