/*jslint browser:true */
/*globals define, $, SITE, _, Expo, TimelineMax */
(function(root, factory) {
  'use strict';
  root.app = root.app || {};
  var o = factory($);
  root.app.VideoPlayerYT = o;
})((window.SITE = window.SITE || {}), function() {
  var ytAPIReady = false;

  // YT Player DL -- Load it....
  function downloadYoutube() {
    // var ytRequired = _.find(SITE.settings.sections.videos.videos, function (item) {
    // 	return item.type == 'youtube' || item.type == 'youtubePlaylist';
    // });
    var ytRequired = true;

    if (!ytRequired) {
      // sequencer.next();
      return;
    }
    var tag = document.createElement('script');
    tag.onerror = function() {
      console.log('script create error');
      // sequencer.next();
    };
    tag.src = '//www.youtube.com/iframe_api';
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  // ...and wait for a response
  function onYouTubeIframeAPIReady() {
    ytAPIReady = true;
    // sequencer.next();
  }

  function VideoPlayerYT(params) {
    this.apiReady = false;
    this.parent = params.parent;
    this.videoData = params.videoData;
    this.player = null;
    this.onReady = params.onReady;
    this.onChange = params.onStateChange || function() {};
    this.load();
  }
  VideoPlayerYT.prototype = {
    load: function() {
      this.parent.empty().append('<div id="ytPlayer"></div>');
      if (!ytAPIReady) {
        $('#ytPlayer').html(
          '<iframe width="100%" height="100%" class="absFull" src="https://www.youtube.com/embed/' +
            this.videoData.videoId +
            '"></iframe>'
        );
        return;
      }
      if (this.player != null) {
        this.player = null;
      }
      if (this.player === null) {
        this.player = new YT.Player('ytPlayer', {
          height: '100%',
          width: '100%',
          'background-color': '#000',
          allowtransparency: 'true',
          videoId: this.videoData.videoId,
          playerVars: {
            autohide: 1,
            autoplay: 1,
            controls: 1,
            enablejsapi: 1,
            modestbranding: 1,
            rel: 0,
            showinfo: 0,
            wmode: 'transparent'
          },
          events: {
            onReady: $.proxy(this.onPlayerReady, this),
            onStateChange: $.proxy(this.onStateChange, this)
          }
        });
      }
    },

    play: function() {
      if (this.player && this.player.playVideo) {
        this.player.playVideo();
      }
    },

    pause: function() {
      if (this.player && this.player.pauseVideo) {
        this.player.pauseVideo();
      }
    },

    onPlayerReady: function() {
      this.onReady();
    },

    onStateChange: function(e) {
      switch (e.data) {
        case 3: // buffering
        case 1: // playing
        case 0:
      }

      this.onChange(e.data);
    },

    destroy: function() {
      this.parent.empty();
      if (this.player != null) {
        this.player = null;
      }
    },

    change: function(newData) {
      this.videoData = newData;
      this.load();
    }
  };

  VideoPlayerYT.downloadYoutube = downloadYoutube;
  VideoPlayerYT.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;

  return VideoPlayerYT;
});

// called by the YT API when its downloaded
function onYouTubeIframeAPIReady() {
  SITE.app.VideoPlayerYT.onYouTubeIframeAPIReady();
}
