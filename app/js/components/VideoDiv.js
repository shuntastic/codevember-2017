/*jslint browser:true */
/*globals define, $ */
/**
 * Home ThreeJS Navigation button.
 * Added via app/HomeNav
 * Data for the button is in the JSON > home > menu
 **/
;(function (root, factory) {
	"use strict";
	root.app = root.app || {};
	var o = factory($);
	root.app.VideoDiv = o;
}(window.SITE = window.SITE || {}, function () {

	/**
	 * Constructor
	 * @param {Type} params
	 */
	function VideoDiv(params) {
		this.bgShell = $('<div class="absFull">').appendTo(params.parent).css({ overflow: 'hidden' });
		this.width = params.width;
		this.height = params.height;
		this.img = $('<img src="'+params.image+'" class="img TL">');
		this.vid = null;
		this.vidSrc = params.video;
		this.muted = typeof params.muted === undefined ? false : params.muted;
		this.autoplay = typeof params.autoplay === undefined ? true : params.autoplay;
		this.preload = params.preload || true;
		this.videoStr = '';
		this.curtainDrawn = false;

		if (params.enabled && params.video != 'NO VIDEO') {
			this.vidLoaded = false;
			this.playing = false;
			this.videoStr = '<video  class="TL"  loop playsinline ' +
				 (this.muted ? 'muted' : '') +' '+
				 (this.preload ? 'preload="auto"' : 'preload="none"' ) + ' ' +
				 (this.autoplay ? 'autoplay' : '' ) +
				 '><source src="'+params.video +'"></video>';
			this.vid = $(this.videoStr);
			this.vid.on('canplay', $.proxy(this.onCanPlay, this));
			this.vid.on('playing', $.proxy(this.onPlay, this));
			this.bgShell.append(this.vid);
		}
		this.bgShell.append(this.img);
		this.bgShell.append($('<div class="absFull curtain">'));
	}

	VideoDiv.prototype = {
		load: function () {
			if (this.vid) {
				this.vidLoaded = true;
				this.vid.get(0).load();
				if (this.muted) {
					this.mute();
				}
			}
			// hack to precache the video for older tablet & mobile devices
			if (sniffer.device != 'desktop' && this.vid
			   && !(sniffer.isiOS && sniffer.v >= 10)) {
				this.play();
				this.stop();
			}
		},

		play: function () {
			this.playing = true;
			if (this.vid) {
				this.playVideo();
			}
		},

		stop: function () {
			this.playing = false;
			if (this.vid) {
				this.vid.get(0).pause();
			}
		},

		playVideo: function () {
			if (this.vid && this.playing) {
				this.vid.attr('autoplay', 'true');
				this.vid.get(0).play();
			}
		},

		onPlay: function() {
			if (this.curtainDrawn) {
				return;
			}

			this.curtainDrawn = true;
			TweenLite.to(this.img, 0.3, { opacity: 0, ease: Linear.easeNone });
			
			//hack for displaying portrait mobile image
			TweenLite.to($('#homeBg .portrait'), 0.3, { opacity: 0, ease: Linear.easeNone });
		},

		onCanPlay: function() {
			if (this.autoplay) {
				this.playVideo();
			}
		},

		resize: function (parW, parH) {
			var w = parW,
				h = parH,
				bgH = h,
				bgW = this.width / this.height * bgH,
				bgL = (w - bgW) * 0.5
			;

			if (bgW < w ) {
				bgW = w;
				bgH = this.height / this.width * bgW;
				bgL = 0;
			}

			TweenLite.set(
				this.img,
				{
					height: bgH,
					width: bgW,
					x: bgL
				}
			);
			if (this.vid) {
				TweenLite.set(
					this.vid,
					{
						height: bgH,
						width: bgW,
						x: bgL
					}
				);
			}
		},

		setSpeed: function(newSpeed) {
			if (this.vid) {
				this.vid.get(0).playbackRate = newSpeed;
			}
		},

		mute: function() {
			this.muted = true;
			if (this.vid) {
				this.vid.prop('muted', true);
			}
		},

		unmute: function() {
			this.muted = false;
			if (this.vid) {
				this.vid.prop('muted', false);
			}
		},

		getTime: function () {
			return this.vid ? this.vid.get(0).currentTime : 0;
		}

	}

	return VideoDiv;
}));
