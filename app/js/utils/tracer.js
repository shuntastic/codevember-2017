(function(window) {
var 
    CAN_TRACE = 0,
    CANNOT_TRACE = 1,
    LIMITED_TRACE = 2,
    DEFAULT_SCOPE = 'tracerDefaultScope',
    
    enabled = true,
    scopes = {},
    inited = false,
    capabilities = CAN_TRACE;
    
function Tracer () {
    inited = true;
    switch (true)
    {
        case !window.console:
        case typeof window.console === 'undefined':
        case !window.console.log:
            capabilities = CANNOT_TRACE;
            break;
            
        case typeof console.log === 'object':
            capabilities = LIMITED_TRACE;
            break
        default:
            capabilities = CAN_TRACE;
    }
}

function traceScope(scope, args) {
    if (!enabled) return;
	trace({ scope: scope, args: _.without(arguments, 0) });
}
	
function trace() {
    if (!enabled) return;
    
    if (arguments.length == 0) trace("TRACER: Trace called with no arguments.");
    
    var args = arguments,
        firstArg = args[0],
        scope = DEFAULT_SCOPE;
    
    if (_.isObject(firstArg) && _.has(firstArg, 'scope') && _.has(firstArg, 'args') && _.isString(firstArg.scope)) {
        scope = firstArg.scope;
        args = (["SCOPE: "+scope + " >> "]).concat(firstArg.args);
        
        if (!checkScope(scope))
            return;
            
    }
    
    switch (capabilities)
    {
        case CAN_TRACE:
            console.log.apply(console, args);
            break;
        case CANNOT_TRACE:
            break;
        case LIMITED_TRACE:
            _.each(args, function (o, i) {
                console.log(o);
            });
            break;
    }
}
    
function checkScope(scope) {
    if (_.has(scopes, scope)) {
        return scopes[scope];
    } else {
        scopes[scope] = true;
        return true;
    }   
}

function enable() {
    if (arguments.length == 0) {
        enabled = true;
    } else if (_.isString(arguments[0]))
        checkScope(arguments[0]);
}

function disable() {
    if (arguments.length == 0) {
        enabled = false;
    } else if (_.isString(arguments[0]) && checkScope(arguments[0]))
        scopes[arguments[0]] = false;
}

window.Tracer = Tracer;
Tracer.prototype.trace = trace;
Tracer.prototype.traceScope = traceScope;
Tracer.prototype.enable = enable;
Tracer.prototype.disable = disable;
window.tracer = new Tracer();
}(window));