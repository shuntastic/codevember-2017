/*jslint browser:true */
/*globals define, $, _, SITE */
;(function (root, factory) {
	"use strict";
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery',
				'underscore',
				'Categorizr'
			],
			function () {
				var o = factory($);
				if (!window.sniffer) {
					window.sniffer = o;
				}
				return (root.utils.sniffer = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.sniffer) {
			window.sniffer = o;
		}
		root.utils.sniffer = o;
	}
}(window.SITE = window.SITE || {}, function () {
	function Sniffer () {
		var ua = navigator.userAgent.toLowerCase();
		this.isSafari = /safari/i.test(navigator.userAgent) && !/chrome/i.test(navigator.userAgent) ;
		this.isFirefox = /firefox/i.test(navigator.userAgent);
		if (/MSIE 8/i.test(navigator.userAgent)) {
			this.v = 8;
			this.isIE = true;
		} else if (/MSIE 9/i.test(navigator.userAgent)) {
			this.v = 9;
			this.isIE = true;
		} else if (/MSIE 10/i.test(navigator.userAgent)) {
			this.v = 10;
			this.isIE = true;
		} else if (/rv:11.0/i.test(navigator.userAgent)) {
			this.v = 11;
			this.isIE = true;
		} else if (/Edge/i.test(navigator.userAgent)) {
			this.v = 12;
			this.isIE = true;
		} else {
			this.isIE = false;
		}

		this.isiOS = /(iPhone|iPad|iPod)/.test(navigator.userAgent);
		if (this.isiOS) {
			var fullV = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
			this.v = parseInt(fullV[1], 10);
		}
		this.isAndroid = /android/i.test(navigator.userAgent);
		this.browser = this.device = categorizr();

		if (document.body.style[ '-webkit-mask-repeat' ] !== undefined) {
			this.cssmask = true;
			$('html').addClass('cssmask');
		} else {
			this.cssMask = false;
			$('html').addClass('no-cssmask');
		}


	}

	return new Sniffer();
}));
