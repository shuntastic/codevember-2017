(function(window) {
  var home,
    me,
    ee,
    wow,
    // slideLoop = false,
    // slideNum = 3,
    // currentSlide = 1,
    myScroll,
    inited = false,
    canvasVideo,
    urlParams = {},
    mainSect,
    sliderTL,
    lastId,
    topMenu,
    topMenuHeight,
    menuItems,
    scrollItems;

  function Home() {
    me = this;
  }

  function init() {
    if (inited) return;
    inited = true;

    // $('#openNavBtn').click(function() {
    //   var active = $(this).hasClass('open');
    //   if (active) {
    //     $('#openNavBtn,#nav,#social').removeClass('open');
    //   } else {
    //     $('#openNavBtn,#nav,#social').addClass('open');
    //   }
    // });
    SITE.sections.prj03.init();
  }

  function closeMenu() {
    if ($('#openNavBtn').hasClass('open')) $('#openNavBtn,#nav,#social').removeClass('open');
  }
  function showPrj(prj) {
    document.getElementById('projects').className = '';
    document.getElementById('projects').className = prj;
  }
  function onResize(sizes) {
    // $('.cover article video').resize(sizes.window.width, sizes.window.height);
    // if (myScroll) {
    //   refreshScroll();
    // }
  }

  function enter() {}

  window.Home = Home;
  window.urlParams = urlParams;
  Home.prototype.init = init;
  Home.prototype.showPrj = showPrj;
  Home.prototype.enter = enter;
  window.home = new Home();
})(window);
