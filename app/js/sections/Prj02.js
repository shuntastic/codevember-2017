(function(root, factory) {
  'use strict';
  root.sections = root.sections || {};

  if (typeof define === 'function' && define.amd) {
    define(
      [
        'jquery',
        'greensock/TweenMax.min',
        'greensock/TimelineMax.min',
        'greensock/easing/EasePack.min',
        'packages/Sequencer'
      ],
      function($, TweenMax, TimelineMax, EasePack, Sequencer) {
        var o = factory($);
        return (root.sections.prj02 = o);
      }
    );
  } else {
    var o = factory($);
    root.sections.prj02 = o;
  }
})((window.SITE = window.SITE || {}), function() {
  var file, audio;
  var context, src, analyser, bufferLength, dataArray, canvas, ctx, WIDTH, HEIGHT, barWidth, barHeight;
  var colorOpt = [
    { r: 255, g: 227, b: 255 },
    { r: 178, g: 141, b: 178 },
    { r: 255, g: 243, b: 201 },
    { r: 161, g: 204, b: 198 },
    { r: 150, g: 178, b: 174 }
  ];

  var empty = { x: 0, y: 0, scale: 1 };
  var images,
    imagesData = [];

  function Prj02() {}
  function enter() {}
  function exit() {}

  function playStream(audioStream) {
    var uInt8Array = new Uint8Array(audioStream);
    var arrayBuffer = uInt8Array.buffer;
    var blob = new Blob([arrayBuffer]);
    var url = URL.createObjectURL(blob);

    audioElement.src = url;
    audioElement.play();
  }
  function postTransition() {}
  function animateAudio() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    WIDTH = canvas.width;
    HEIGHT = canvas.height;
    // empty = { x: WIDTH, y: HEIGHT, scale: 1 };
    // barWidth = WIDTH / bufferLength * 1;
    // barHeight;
    x = 0;

    analyser.getByteFrequencyData(dataArray);

    var grd = ctx.createLinearGradient(0, 0, 0, HEIGHT);
    grd.addColorStop(0, '#94ccd1');
    grd.addColorStop(0.77, '#579399');
    grd.addColorStop(1, '#6393b7');
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    for (var i = 0; i < bufferLength; i++) {
      if (dataArray[i] > 230) {
        //SET VOLUME THRESHOLD
        barWidth = dataArray[i] * 1.5;
        // var imgRef =
        barHeight = HEIGHT / dataArray[i] * (HEIGHT / 4);
        var colorIndex = Math.min(colorOpt.length - 1, Math.floor(Math.random() * colorOpt.length));
        // i == 0 ? console.log(i, dataArray[i]) : null;
        var r = colorOpt[colorIndex].r;
        var g = colorOpt[colorIndex].g;
        var b = colorOpt[colorIndex].b;
        var a = 1;
        // var a = dataArray[i] / 255;
        // console.log('a', a);
        ctx.fillStyle = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
        ctx.fillRect(x, 0, barWidth, HEIGHT);

        // x = Math.min(Math.random() * WIDTH - barWidth, Math.random() * WIDTH);
      } else {
        // } else if (dataArray[i] > 100) {
        barWidth = dataArray[i] * 1.5;

        ctx.fillStyle = 'rgba(' + colorOpt[3].r + ',' + colorOpt[3].g + ',' + colorOpt[3].b + ',' + 1 + ')';

        ctx.fillRect(x, 0, barWidth, HEIGHT);
      }

      x *= WIDTH * barWidth;
    }
  }

  function renderFrame() {
    requestAnimationFrame(renderFrame);
    animateAudio();
  }

  function init() {
    home.showPrj('prj02');

    // var files = document.getElementById('thefiles').files;
    // var uInt8Array = new Uint8Array();
    // var arrayBuffer = uInt8Array.buffer;
    // var blob = new Blob([arrayBuffer]);
    var audio = new Audio(SITE.dom + 'aud/Ballerina.mp3');
    // audio.src = URL.createObjectURL();
    audio.load();
    audio.onended = function() {
      document.getElementById('shunLogo').className += 'done';
    };
    context = new AudioContext();
    src = context.createMediaElementSource(audio);
    analyser = context.createAnalyser();

    src.connect(analyser);
    analyser.connect(context.destination);

    analyser.fftSize = 256;

    bufferLength = analyser.frequencyBinCount;
    // console.log(bufferLength);

    dataArray = new Uint8Array(bufferLength);
    canvas = document.getElementById('canvas2');
    ctx = canvas.getContext('2d');
    audio.play();
    audio.volume = 1.0;
    renderFrame();
  }

  Prj02.prototype.init = init;
  Prj02.prototype.enter = enter;
  Prj02.prototype.exit = exit;

  return new Prj02();
});
