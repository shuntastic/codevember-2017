(function(root, factory) {
  'use strict';
  root.sections = root.sections || {};

  if (typeof define === 'function' && define.amd) {
    define(
      [
        'jquery',
        'greensock/TweenMax.min',
        'greensock/TimelineMax.min',
        'greensock/easing/EasePack.min',
        'packages/Sequencer'
      ],
      function($, TweenMax, TimelineMax, EasePack, Sequencer) {
        var o = factory($);
        return (root.sections.prj03 = o);
      }
    );
  } else {
    var o = factory($);
    root.sections.prj03 = o;
  }
})((window.SITE = window.SITE || {}), function() {
  var file, audio;
  var app,
    container,
    audctx,
    src,
    analyser,
    analyserLeft,
    analyserRight,
    bufferLength,
    dataArray,
    canvas,
    ctx,
    WIDTH,
    HEIGHT,
    barWidth,
    barHeight;
  var colorOpts = [
    { r: 255, g: 227, b: 255, hex: 0x518084 },
    { r: 178, g: 141, b: 178, hex: 0xd9afc7 },
    { r: 255, g: 243, b: 201, hex: 0x9e9660 },
    { r: 161, g: 204, b: 198, hex: 0x847f57 },
    { r: 150, g: 178, b: 174, hex: 0x3a5051 }
  ];
  var imageOpts = [
    SITE.dom + 'images/buildings/Layer3.png',
    SITE.dom + 'images/buildings/Layer4.png',
    SITE.dom + 'images/buildings/Layer5.png',
    SITE.dom + 'images/buildings/Layer6.png',
    SITE.dom + 'images/buildings/Layer7.png',
    SITE.dom + 'images/buildings/Layer8.png',
    SITE.dom + 'images/buildings/Layer9.png',
    SITE.dom + 'images/buildings/Layer10.png',
    SITE.dom + 'images/buildings/Layer11.png',
    SITE.dom + 'images/buildings/Layer12.png',
    SITE.dom + 'images/buildings/Layer13.png',
    SITE.dom + 'images/buildings/Layer14.png',
    SITE.dom + 'images/buildings/Layer15.png',
    SITE.dom + 'images/buildings/Layer16.png',
    SITE.dom + 'images/buildings/Layer17.png',
    SITE.dom + 'images/buildings/Layer18.png',
    SITE.dom + 'images/buildings/Layer19.png',
    SITE.dom + 'images/buildings/Layer20.png',
    SITE.dom + 'images/buildings/Layer21.png',
    SITE.dom + 'images/buildings/Layer22.png',
    SITE.dom + 'images/buildings/Layer23.png',
    SITE.dom + 'images/buildings/Layer24.png',
    SITE.dom + 'images/buildings/Layer25.png',
    SITE.dom + 'images/buildings/Layer26.png',
    SITE.dom + 'images/buildings/Layer27.png',
    SITE.dom + 'images/buildings/Layer28.png',
    SITE.dom + 'images/buildings/Layer29.png',
    SITE.dom + 'images/buildings/Layer30.png',
    SITE.dom + 'images/buildings/Layer31.png',
    SITE.dom + 'images/buildings/Layer32.png',
    SITE.dom + 'images/buildings/Layer33.png',
    SITE.dom + 'images/buildings/Layer34.png',
    SITE.dom + 'images/buildings/Layer35.png',
    SITE.dom + 'images/buildings/Layer36.png',
    SITE.dom + 'images/buildings/Layer37.png',
    SITE.dom + 'images/buildings/Layer38.png',
    SITE.dom + 'images/buildings/Layer39.png',
    SITE.dom + 'images/buildings/Layer40.png',
    SITE.dom + 'images/buildings/Layer41.png',
    SITE.dom + 'images/buildings/Layer42.png',
    SITE.dom + 'images/buildings/Layer43.png',
    SITE.dom + 'images/buildings/Layer44.png',
    SITE.dom + 'images/buildings/Layer45.png',
    SITE.dom + 'images/buildings/Layer46.png',
    SITE.dom + 'images/buildings/Layer47.png',
    SITE.dom + 'images/buildings/Layer48.png'
  ];

  var empty = { x: 0, y: 0, scale: 1 };

  var images,
    imagesData = [];

  function Prj03() {}
  function enter() {}
  function exit() {}

  function preload(imgArray) {
    images = new Array();
    for (i = 0; i < imgArray.length; i++) {
      images[i] = new Image();
      images[i].src = imgArray[i];
    }
  }

  function playStream(audioStream) {
    var uInt8Array = new Uint8Array(audioStream);
    var arrayBuffer = uInt8Array.buffer;
    var blob = new Blob([arrayBuffer]);
    var url = URL.createObjectURL(blob);

    audioElement.src = url;
    audioElement.play();
  }
  function getAudioChannels() {
    audctx.decodeAudioData(someStereoBuffer, function(data) {
      var source = audctx.createBufferSource();
      source.buffer = data;
      var splitter = audctx.createChannelSplitter(2);
      source.connect(splitter);
      // var merger = audctx.createChannelMerger(2);

      // Reduce the volume of the left channel only
      // var gainNode = audctx.createGain();
      // gainNode.gain.value = 0.5;
      splitter.connect(gainNode, 0);
    });
  }
  function addBuilding(ref) {
    imagesData[ref] = true;
    bldngRef = Math.floor(Math.random() * images.length);
    var bldng = PIXI.Sprite.fromImage(imageOpts[bldngRef]);
    // console.log('ref', ref);
    bldng.height = dataArray[i] * (i > 15 ? 1.25 : 0.35);
    bldng.width = bldng.height * Math.max(1.2, Math.random() * 2);
    // bldng.alpha = Math.min(0.5, Math.random());
    bldng.alpha = 0.5;
    // bldng.blendMode = PIXI.BLEND_MODES.MULTIPLY;
    bldng.x = WIDTH + bldng.width - Math.random() * 10;
    bldng.y = HEIGHT - bldng.height;

    var rectangle = new PIXI.Graphics();
    var colorIndex = colorOpts[Math.min(colorOpts.length - 1, Math.floor(Math.random() * colorOpts.length))].hex;
    rectangle.beginFill(colorIndex);
    rectangle.drawRect(bldng.x, bldng.y, bldng.width, bldng.height);
    rectangle.endFill();

    // var blurFilter = new PIXI.filters.BlurFilter();
    // var rectblurFilter = new PIXI.filters.BlurFilter();
    // bldng.filters = [blurFilter];
    // rectangle.filters = [rectblurFilter];
    // blurFilter.blur = 1;
    // rectblurFilter.blur = 2;

    app.stage.addChild(rectangle);
    app.stage.addChild(bldng);

    // Listen for animate update
    app.ticker.add(function(delta) {
      try {
        if (bldng.x < -(bldng.width + 5)) {
          imagesData[ref] = false;
          app.stage.removeChild(rectangle);
          app.stage.removeChild(bldng);
          app.ticker.remove(this);
          // bldng.destroy();
          // rectangle.destroy();
        } else {
          bldng.x -= bldng.width / 3;
          rectangle.x -= bldng.width / 3;
        }
      } catch (e) {
        console.log('error:', e);
      }
    });
  }

  function animateAudio() {
    WIDTH = window.innerWidth - 40;
    HEIGHT = WIDTH * 0.3623;
    app.renderer.resize(WIDTH, HEIGHT);
    // HEIGHT = window.innerHeight;
    analyser.getByteFrequencyData(dataArray);

    for (var i = 0; i < bufferLength; i++) {
      if (dataArray[i] > 220) {
        // && !imagesData[i].running
        var bldng = imagesData[i];
        if (!imagesData[i]) {
          addBuilding(i);
        }
      }
    }
  }

  function renderFrame() {
    requestAnimationFrame(renderFrame);
    animateAudio();
  }

  function init() {
    home.showPrj('prj03');
    preload(imageOpts);
    for (var i = 0; i < imageOpts.length; i++) {
      imagesData.push(false);
    }
    // var files = document.getElementById('thefiles').files;
    // var uInt8Array = new Uint8Array();
    // var arrayBuffer = uInt8Array.buffer;
    // var blob = new Blob([arrayBuffer]);
    var audio = new Audio(SITE.dom + 'aud/Ballerina.mp3');
    // var audio = new Audio(SITE.dom + 'aud/RoadToPerdition.mp3');

    // audio.src = URL.createObjectURL();
    // audio.onended = function() {
    //   document.getElementById('shunLogo').className += 'done';
    // };

    audio.load();
    audctx = new AudioContext();
    src = audctx.createMediaElementSource(audio);
    analyser = audctx.createAnalyser();
    src.connect(analyser);
    analyser.connect(audctx.destination);
    analyser.fftSize = 256;
    bufferLength = analyser.frequencyBinCount;
    // console.log(bufferLength);
    dataArray = new Uint8Array(bufferLength);

    app = new PIXI.Application(window.innerWidth - 40, (window.innerWidth - 40) * 0.3623, {
      transparent: true,
      antialias: true
    });
    PIXI.settings.RESOLUTION = window.devicePixelRatio;
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

    document.getElementById('content3').appendChild(app.view);
    container = new PIXI.Container();
    app.stage.addChild(container);

    audio.play();
    audio.volume = 1.0;

    renderFrame();
  }

  Prj03.prototype.init = init;
  Prj03.prototype.enter = enter;
  Prj03.prototype.exit = exit;

  return new Prj03();
});
