var trackingEnabled = true;
function trackPageView(path) {
	if (trackingEnabled) {
		if (typeof ga !== 'undefined') {
			ga('send', 'pageview', path);
			ga(SITE.settings.gaName2 +'.send', 'pageview', path);
		}
	} else {
		console.log('send', 'pageview', path);
	}
}

function trackEvent(cat, action, label) {
	if (trackingEnabled) {
		if (typeof ga !== 'undefined') {
			ga('send', 'event', cat, action, label);
			ga(SITE.settings.gaName2 +'.send', 'event', cat, action, label);
		}
	} else {
		console.log('send', 'event', cat, action, label);
	}
}
