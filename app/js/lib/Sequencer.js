;
(function (root, factory) {
	root.utils = root.utils || {};

	if (typeof define === 'function' && define.amd) {
		define(
			[
				'jquery'
			],
			function () {
				var o = factory($);
				if (!window.sequencer)
					window.sequencer = o;
				return (root.utils.sequencer = o);
			}
		);
	} else {
		var o = factory($);
		if (!window.sequencer)
			window.sequencer = o;
		root.utils.sequencer = o;
	}
}(window.SITE = window.SITE || {}, function () {
	var tasks = [],
		delay = 60;

	function Sequencer() {}

	function add(arr, autorun) {
		var tasksL = tasks.length;
		var i = arr.length;
		while (i) {
			i--;
			tasks.unshift(arr[i]);
		}
		if (typeof autorun !== undefined || autorun)
			executeNextStep();
	}

	function executeNextStep() {
		if (tasks.length === 0) {
			return;
		}
		var step = tasks.shift();
		if (step === null) {
			return;
		}

		var func = step.func;
		if (typeof step.vars !== 'undefined')
			func.apply(null, step.vars);
		else
			func();
	}

	function next(delay) {
		if (tasks.length)
			setTimeout(executeNextStep, 60);
	}

	function clear() {
		tasks = [];
	}

	function setDelay(value) {
		delay = value;
	}

	Sequencer.prototype.add = add;
	Sequencer.prototype.next = next;
	Sequencer.prototype.clear = clear;
	Sequencer.prototype.executeNextStep = executeNextStep;
	Sequencer.prototype.setDelay = setDelay;
	return new Sequencer();

}));