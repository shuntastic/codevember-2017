<?php

require_once('php/Mobile_Detect.php');
$detect = new Mobile_Detect;

// get the domain and path to this file
$domain = ((!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "") ? 'http://' : 'https://').$_SERVER['HTTP_HOST'];
$path = $domain.strtok(dirname($_SERVER["PHP_SELF"])."/",'?');
$slashes = substr($path,-2);
if($slashes === '//') $path = substr($path,0,strlen($path)-1);
$path = stripslashes($path);

$jsSrc = file_get_contents('json/settings.json');
$jsonData = json_decode($jsSrc, true);

$url = $jsonData['metadata']['url'];
$title = $jsonData['metadata']['title'];
$desc = $jsonData['metadata']['desc'];
$keywords = $jsonData['metadata']['keywords'];
$thumb = $path.$jsonData['thumb'];
$favicon = $path.$jsonData['favicon'];
$projectInfo = $jsonData['projects'];
$social = $jsonData['social'];
$client = $jsonData['pageHeader']['client'];

?>

<!DOCTYPE html>
<html class="no-js <?php if( $detect->isMobile()) { if (!$detect->isTablet()) { ?>phone mobile<?php } else { ?>tablet no-phone mobile<?php } } else { ?>desktop no-phone<?php } ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title><?php echo $title; ?></title>

	<meta name="title" content="<?php echo $title; ?>" />
	<meta name="description" content="<?php echo $desc; ?>" />

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo $path; ?>faviconsapple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $path; ?>favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $path; ?>favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $path; ?>favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $path; ?>favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $path; ?>favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $path; ?>favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $path; ?>favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $path; ?>favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $path; ?>favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $path; ?>favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $path; ?>favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path; ?>favicon/favicon-16x16.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $path; ?>favicon/favicon-16x16.png">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<meta name="keywords" content="<?php echo $keywords; ?>" />
	<link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">
	<meta property="og:title" content="<?php echo $title; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="<?php echo $thumb; ?>" />
	<meta property="og:description" content="<?php echo $desc; ?>" />
	<meta property="og:site_name" content="<?php echo $path; ?>"/>

	<meta itemprop="name" content="<?php echo $title; ?>">
	<meta itemprop="description" content="<?php echo $descr; ?>">
	<meta itemprop="image" content="<?php echo $thumb; ?>">

	<!-- <link rel="image_src" type="image/jpeg" href="./img/share.jpg" /> -->
	<link rel="stylesheet" href="css/main.css">
	<script>
		var SITE = SITE || {};
		SITE.dom = '<?php echo $path; ?>';
	</script>

	<!-- build:remove -->
	<script src="//localhost:35729/livereload.js"></script>
	<!-- /build -->
	<script>
		if (!window.requestAnimationFrame) {
			window.requestAnimationFrame = ( function() {
				return window.webkitRequestAnimationFrame ||
				window.mozRequestAnimationFrame ||
				window.oRequestAnimationFrame ||
				window.msRequestAnimationFrame ||
				function(callback,element) {
					window.setTimeout(callback,1000/60);
				};
			} )();
		}

		// var context,audioSource,cnvs;
		// var draw = function() {
		// for(bin = 0; bin < audioSource.streamData.length; bin ++) {
		// 	// do something with each value. Here's a simple example
		// 	var val = audioSource.streamData[bin];
		// 	var red = val;
		// 	var green = 255 - val;
		// 	var blue = val / 2; 
		// 	context.fillStyle = 'rgb(' + red + ', ' + green + ', ' + blue + ')';
		// 	context.fillRect(bin * 2, 0, 2, 200);
		// 	// use lines and shapes to draw to the canvas is various ways. Use your imagination!
		// }

		// requestAnimationFrame(draw);
		// };
	</script>

</head>

<body style="display:none;">
	<?php
	// ADDS OPTIONAL STYLES AND ANIMATION TO OVERALL CONTENT AREA
	$props = $jsonData['properties'];
	$style = ($props['style']) ? ' style="'.$props['style'].'"' : '';

	?>
	<div id="codevember" class="TL absFull">
		<div id="headerShell" class="TL fullW">
			<div id="header" class="TL absFull">
				<nav id="nav" class="TR">
				<?php foreach($projectInfo as $value) { ?>
					<span><a href="#<?php echo $value['id'] ?>"><?php echo $value['menu'] ?></a></span>
				<?php } ?>
				</nav>
				<div id="social" class="TR hidden">
				<?php foreach($social as $value) { ?>
					<a href="<?php echo $value['link'] ?>" target="_blank" class="icon-<?php echo $value['site'] ?>"></a>
				<?php } ?>
				</div>
			</div>
			<div id="openNavBtn" class="TR">
				<div class="row row1 TL"></div>
				<div class="row row2 TL"></div>
				<div class="row row3 TL"></div>
				<div class="row row4 TL"></div>
				<div class="row row5 TR"></div>
			</div>
		</div>
		<div id="projects">
			<div id="projectWrapper">
				<?php
				$val = '';
				$newSection = '';
				$mainctr = 1;
				$ctr = 1;
				foreach($projectInfo as $value) {
					$pagecontent = file_get_contents($value['template']);
					$newSection .= '<div id="'.$value['id'].'" class="prj">'.$pagecontent.'</div>';
				}
				echo $newSection;
				?>
			</div>
		</div>	
	</div>
	<div id="contactForm" class="overlay">
		<div id="contactFormWrapper" class="overlayWrapper formWrapper">
			<h1>Whatcha Need?</h1>
				<?php //include('php/contact.php'); ?>
			<div class="closeBtn TR"></div>
		</div>
	</div>
	<script type="text/javascript">
		SITE.settings = <?php echo(preg_replace('/\t|\r|\n/', '', $jsonData)) ?>;
	</script>

	<!--[if gt IE 9]><!-->
	<!-- build:js /js/main.js-->
	<script type="text/javascript" data-main="js/main" src="js/packages/requirejs/require.js"></script>
	<!-- /build-->
	<!--<![endif]-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-18784661-9', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
